<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>

 <link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="form-area">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
				
			<form action="newMessage" method="post">
				<br />
				<label for="subject">件名(30文字まで)</label> 
				<input name="subject" />
				<br />
				<label for="category">カテゴリー(10文字まで)</label> 
				<input name="category"  />
				<br /> 本文（1000文字まで）
				<textarea name="text" cols="100" rows="5" class="tweet-box">
				</textarea>
				<br /> 
				<input type="submit" value="投稿">
			</form>
			
		</div>
		<a href="./">戻る</a>
		
		<div class="copyright">Copyright(c)2018wasai</div>
	</div>
</body>
</html>