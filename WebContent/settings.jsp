<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body onLoad="functionName()">
    
    <script type = "text/javascript">
	    function functionName()
	    {
	        var select1 = document.forms.formName.branchCode; 
	        var select2 = document.forms.formName.departmentCode; 
	         
	        select2.options.length = 0; 
	         
	        if (select1.options[select1.selectedIndex].value == "1")
	            {
	                select2.options[0] = new Option("総務","1");
	                select2.options[1] = new Option("情報","4");
	            }
	         
	        else if (select1.options[select1.selectedIndex].value == "2")
	            {
	                select2.options[0] = new Option("店長","2");
	                select2.options[1] = new Option("社員","3");
	            }
	    }
	</script>
    
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="settings" method="post"  name="formName"><br />
                <input name="id" value="${editUser.id}"  type="hidden"/>
                <label for="loginId">ログインID</label>
                <input name="loginId" value="${editUser.loginId}" /><br />

                <label for="name">ユーザー名</label>
                <input name="name" value="${editUser.name}" /><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" /> <br />
                
                <label for="password">パスワード確認</label> 
				<input name="checkPassword"type="password" /> 

				<p>
					<select name = "branchCode" onChange="functionName()">
						<option value = "1">本店</option>
						<option value = "2">支店</option>
					</select>
				</p>
				<p>
					<select name = "departmentCode">
					</select>
				</p>
                <input type="submit" value="登録" /> <br />
                <a href="admin">戻る</a>
            </form>
            <div class="copyright"> Copyright(c)2018wasai</div>
        </div>
    </body>
</html>