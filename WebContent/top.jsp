<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ホーム</title>
	
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<a href="newMessage">投稿</a> 
				<a href="admin">管理画面</a> 
				<a href="logout">ログアウト</a>
			</div>
			<c:if test="${ not empty errorMessages }">
	        	<div class="errorMessages">
	            	<ul>
	                	<c:forEach items="${errorMessages}" var="message">
	                    	<li><c:out value="${message}" />
	                    </c:forEach>
	                </ul>
	            </div>
	            <c:remove var="errorMessages" scope="session"/>
	        </c:if>
			
			<form action="" method="get">
				投稿一覧 
				<br />
			 	期間指定 
			 	<br /> 
			 	<input name="firstDate"  type="date"></input>
			 	<input name="secondDate" type="date"></input> 
			 	<label for="categorySearch">
			 	カテゴリー検索
			 	</label> 
			 	<input name="categorySearch"  type="search"></input> 
			 	<input type="submit" value="検索" />
			 	<input type="submit" value="一覧表示" />
			 </form>
	
			<div class="messages">
				<c:forEach items="${messages}" var="message">
					<div class="message">
						<div class="login_name">
						投稿者 
							<span class="name"><c:out value="${message.name}" /></span>
						</div>
						件名
						<form action="" method="get">
							<c:if test="${message.userId==loginUser.id||loginUser.departmentCode==4}">
								<button type="submit" name="messageDelete" value="${message.id}">削除</button>
							</c:if>
						</form>
						<div class="text">
							<c:out value="${message.subject}" />
						</div>
						カテゴリ
						<div class="text">
							<c:out value="${message.category}" />
						</div>
						本文
						<div class="text">
							<c:out value="${message.text}" />
						</div>
						<div class="date">
							<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
						</div>
						
						コメント一覧
						<c:forEach items="${comments}" var="comment">
							<c:if test="${comment.messageId == message.id}" >
								<div class="comment">
									<div class="login-name">
										<span class="name"><c:out value="${comment.name}" /></span>
									</div>
									<div class="text">
										<c:out value="${comment.content}" />
									</div>
									<div class="date">
										<fmt:formatDate value="${comment.createdDate}"
											pattern="yyyy/MM/dd HH:mm:ss" />
										<form action="" method="get">
											<c:if test="${comment.userId==loginUser.id||loginUser.departmentCode==4}">
												<button type="submit" name="commentDelete" value="${comment.id}">削除</button>
											</c:if>
										</form>
									</div>
								</div>
							</c:if>
						</c:forEach>
					</div>
					<form action="newComment" method="post">
						<br /> コメント（500文字まで）
							<textarea name="commentContent" cols="100" rows="5" class="tweet-box"></textarea>
						<br /> 
						<input name="messageId" type="hidden" value="${message.id}">	
						<input type="submit" value="送信">
					</form>
				</c:forEach>
			</div>
			<div class="copyright">Copyright(c)2018wasai</div>
		</div>
	</body>
</html>