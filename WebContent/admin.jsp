<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理</title>

<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<script>
		function submitChk() {
			var flag = confirm("変更してもよろしいですか？\n\n変更したくない場合は[キャンセル]ボタンを押して下さい");
			return flag;
		}
	</script>

	<div class="main-contents">
		<div class="header">
			<a href="signup">登録</a> 
			<a href="index.jsp">ホーム</a>
		</div>
		
        <table  border="1" >
        <caption>登録者一覧</caption>
        <tr>
	        <th>ログインID</th>
	        <th>名前</th>
	        <th>支店</th>
	        <th>部署・役職</th>
	        <th></th>
	        <th></th>
	        <th></th>
	    </tr>
		<c:forEach items="${users}" var="user">
			<tr>
				<td align="center" ><c:out value="${user.loginId}" /></td>
				<td align="center"><c:out value="${user.name}" /></td>
				<td align="center"><c:out value="${branches[user.branchCode]}" /></td>
				<td align="center"><c:out value="${departments[user.departmentCode]}" /></td>
					
				<td align="center">
					<form action="settings"  name="settings"  method="get">
					<input type="hidden" name="userId" value="${user.id}">
					<input type="submit"  value="編集">
					</form>
				</td>
				<td align="center">	
					<form action="admin" method="post">
						<c:if test="${user.pauseUser==0}">
							<button type="submit" name="userPause" value="${user.id}"
								onclick="return submitChk()">停止</button>
						</c:if>
						<c:if test="${user.pauseUser==1}">
							<button type="submit" name="userRevive" value="${user.id}"
								onclick="return submitChk()">復活</button>
						</c:if>
					</form>
				</td>
				<td>
					<c:if test="${user.pauseUser==1}">
							※停止中
					</c:if>
				</td>
				
		</c:forEach>
		 </table>
	</div>
	<div class="copyright">Copyright(c)2018wasai</div>

</body>
</html>