package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Message;
import bbs.beans.User;
import bbs.service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("message.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> errorMessages = new ArrayList<String>();

        if (isValid(request, errorMessages) == true) {

           User user = (User) session.getAttribute("loginUser");

            Message messages = new Message();
            messages.setSubject(request.getParameter("subject"));
            messages.setText(request.getParameter("text"));
            messages.setCategory(request.getParameter("category"));
            messages.setUserId(user.getId());
            
            new MessageService().register(messages);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("newMessage");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

    	String subject = request.getParameter("subject");
    	String text = request.getParameter("text");
    	String category = request.getParameter("category");

        if (StringUtils.isEmpty(subject) ) {
            errorMessages.add("件名を入力してください");
        }

        if (StringUtils.isEmpty(text) ) {
            errorMessages.add("本文を入力してください");
        }

        if (StringUtils.isEmpty(category) ) {
            errorMessages.add("カテゴリーを入力してください");
        }
        if (30 < subject.length()) {
            errorMessages.add("件名は30文字以下で入力してください");
        }
        if (10 < category.length()) {
            errorMessages.add("カテゴリーは10文字以下で入力してください");
        }
        if (1000 < text.length()) {
            errorMessages.add("本文は1000文字以下で入力してください");
        }
        if (errorMessages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
