package bbs.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import bbs.beans.UserComment;
import bbs.beans.UserMessage;
import bbs.service.CommentService;
import bbs.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {

		String searchCategory = request.getParameter("categorySearch");
		String firstDate = request.getParameter("firstDate");
		String secondDate = request.getParameter("secondDate");
		String messageDelete = request.getParameter("messageDelete");
		String commentDelete = request.getParameter("commentDelete");

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		if (StringUtils.isEmpty(firstDate)) {
			firstDate = sdf.format(2018 - 07 - 01);
		}
		
		if (StringUtils.isEmpty(secondDate)) {
			secondDate = sdf.format(Calendar.getInstance().getTime());
		}

		List<UserMessage> messages = new MessageService().getMessage(searchCategory, firstDate, secondDate);
		request.setAttribute("messages", messages);

		List<UserComment> comments = new CommentService().getComment(commentDelete);
		request.setAttribute("comments", comments);

		new MessageService().deleteMessage(messageDelete);
		new CommentService().deleteComment(commentDelete);
		
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}

}
