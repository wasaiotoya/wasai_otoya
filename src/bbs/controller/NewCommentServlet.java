package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Comment;
import bbs.beans.User;
import bbs.service.CommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<>();
        if (isValid(request, messages) == true) {
        	User loginUser = (User) session.getAttribute("loginUser");
          
            Comment comment = new Comment();
            comment.setContent(request.getParameter("commentContent"));
            comment.setUserId(loginUser.getId());
            comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));
            
            new CommentService().register(comment);
            response.sendRedirect("./");
            return;
        }
        session.setAttribute("errorMessages", messages);
        response.sendRedirect("./");
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String content = request.getParameter("commentContent");

        if (StringUtils.isEmpty(content)){
        	messages.add("コメントを入力してください");
        }
        
        if (500 < content.length()){
        	messages.add("500文字以下で入力してください");
        }
        
        if (StringUtils.isEmpty(request.getParameter("messageId"))){
        	messages.add("投稿IDが指定されていません");
        }
        
        if (messages.size() == 0) {
        	return true;
        } else {
            return false;
        }
    }
        
}

