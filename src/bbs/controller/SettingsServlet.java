package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.User;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {

		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		if (loginUser.getBranchCode() != 1 || loginUser.getDepartmentCode() != 1) {
			List<String> errorMessages = new ArrayList<>();
			errorMessages.add("管理者権限がありません");

			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		int userId = Integer.parseInt(request.getParameter("userId"));
		User editUser = new UserService().getEditUser(userId);

		request.setAttribute("editUser", editUser);
		request.getRequestDispatcher("settings.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();
		HttpSession session = request.getSession();

		int id = Integer.parseInt(request.getParameter("id"));
		User editUser = new UserService().getEditUser(id);

		if (isValid(request, errorMessages) == true) {

			editUser.setId(Integer.parseInt(request.getParameter("id")));
			editUser.setLoginId(request.getParameter("loginId"));
			editUser.setName(request.getParameter("name"));
			editUser.setPassword(request.getParameter("password"));
			editUser.setBranchCode(Integer.parseInt(request.getParameter("branchCode")));
			editUser.setDepartmentCode(Integer.parseInt(request.getParameter("departmentCode")));

			new UserService().update(editUser);

			response.sendRedirect("admin");
		} else {
			session.setAttribute("errorMessages", errorMessages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String checkPassword = request.getParameter("checkPassword");

		if (StringUtils.isEmpty(loginId)) {
			errorMessages.add("ログインIDを入力してください");
		}
		if (!loginId.matches("[0-9a-zA-Z]{6,20}") && StringUtils.isNotEmpty(loginId)) {
			errorMessages.add("IDは6文字以上、20文字以下の半角英数字で入力してください");
		}
		if (name.length() > 10 && StringUtils.isNotEmpty(name)) {
			errorMessages.add("ユーザー名は10文字以下で入力してください");
		}
		 if(!password.matches("[ -~]{6,20}")&&StringUtils.isNotEmpty(password)){
	        	errorMessages.add("パスワードは6文字以上、20文字以下の半角英数字・記号で入力してください");
	        }
		 if (!checkPassword.equals(password)&&StringUtils.isNotEmpty(password)) {
				errorMessages.add("パスワードが間違っています");
			}
		if (errorMessages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}