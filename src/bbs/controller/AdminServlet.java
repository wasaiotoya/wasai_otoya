package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.User;
import bbs.service.UserService;
import bbs.utils.Branches;
import bbs.utils.Departments;

@WebServlet(urlPatterns = { "/admin" })
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {

		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		
		if (loginUser.getBranchCode() != 1 || loginUser.getDepartmentCode() != 1) {
			List<String> errorMessages = new ArrayList<>();
			errorMessages.add("管理者権限がありません");
			
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}
		
		List<User> users = new UserService().getUsers();

		request.setAttribute("branches", Branches.getInstance());
		request.setAttribute("departments", Departments.getInstance());
		request.setAttribute("users", users);
		request.getRequestDispatcher("admin.jsp").forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {
		
		String pauseUser = request.getParameter("userPause");
		String reviveUser = request.getParameter("userRevive");
		
		new UserService().pauseUser(pauseUser);
		new UserService().reviveUser(reviveUser);

		response.sendRedirect("admin");
	}
}
