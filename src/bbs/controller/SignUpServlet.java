package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.User;
import bbs.service.UserService;
import bbs.utils.Branches;
import bbs.utils.Departments;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
    	
    	 HttpSession session = request.getSession();
    	 User loginUser = (User)session.getAttribute("loginUser");
    	 
    	 if(loginUser.getBranchCode()!=1||loginUser.getDepartmentCode()!=1){
    		 List<String> errorMessages = new ArrayList<>();
    		 errorMessages.add("管理者権限がありません");
    		 
    		 session.setAttribute("errorMessages", errorMessages);
    		 response.sendRedirect("./");
    		 return;
    	 }
    	
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
    	
        List<String> errorMessages = new ArrayList<String>();
        HttpSession session = request.getSession();
        
        if (isValid(request, errorMessages) == true) {

            User user = new User();
            user.setLoginId(request.getParameter("loginId"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setBranchCode(Integer.parseInt(request.getParameter("branchCode")));
            user.setDepartmentCode(Integer.parseInt(request.getParameter("departmentCode")));
           
            new UserService().register(user);
            
            request.setAttribute("branches", Branches.getInstance());
    		request.setAttribute("departments", Departments.getInstance());
           
            response.sendRedirect("admin");
        } else {
            session.setAttribute("errorMessages", errorMessages);
            
            String loginId = request.getParameter("loginId");
            String name = request.getParameter("name");
            String branchCode = request.getParameter("branchCode");
            String departmentCode = request.getParameter("departmentCode");
            
            request.setAttribute("loginId",loginId); 
            request.setAttribute("name",name); 
            request.setAttribute("branchCode",branchCode); 
            request.setAttribute("departmentCode",departmentCode); 
            
            request.getRequestDispatcher("signup.jsp").forward(request, response);
            response.sendRedirect("signup");
        }
    }
    
    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");
        String checkPassword = request.getParameter("checkPassword");
        String name = request.getParameter("name");
        
        if (StringUtils.isEmpty(loginId) ) {
            messages.add("IDを入力してください");
        }
        if (StringUtils.isEmpty(password) ) {
            messages.add("パスワードを入力してください");
        }
        if (StringUtils.isEmpty(name) ) {
            messages.add("ユーザー名を入力してください");
        }
        if (!loginId.matches("[0-9a-zA-Z]{6,20}") && StringUtils.isNotEmpty(loginId)) {
			messages.add("IDは6文字以上、20文字以下の半角英数字で入力してください");
		}
        if(!password.matches("[ -~]{6,20}")&&StringUtils.isNotEmpty(password)){
        	messages.add("パスワードは6文字以上、20文字以下の半角英数字・記号で入力してください");
        }
        if(name.length()>10&&StringUtils.isNotEmpty(name)){
        	messages.add("ユーザー名は10文字以下で入力してください");
        }
        if(!checkPassword.equals(password)){
        	messages.add("パスワードが間違っています");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
