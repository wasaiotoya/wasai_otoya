package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.UserComment;
import bbs.exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComment(Connection connection,String commentDelete) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.comments_content as comments_content, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.name as name, ");
            sql.append("comments.message_id as message_id, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<>();
        
        try {
            while (rs.next()) {
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                int messageId = rs.getInt("message_id");
                String content = rs.getString("comments_content");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserComment comment = new UserComment();
                comment.setId(id);
                comment.setLoginId(loginId);
                comment.setName(name);
                comment.setUserId(userId);
                comment.setContent(content);
                comment.setMessageId(messageId);
                comment.setCreatedDate(createdDate);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public void deleteUserComments(Connection connection, String commentDelete) {
		PreparedStatement ps = null;
		try {

			String sql = "DELETE  FROM  comments WHERE id=?";
			ps = connection.prepareStatement(sql);

			ps.setString(1, commentDelete);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
    
}