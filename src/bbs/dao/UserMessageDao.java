package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bbs.beans.UserMessage;
import bbs.exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection, String search_category, String firstDate,String secondDate) {
		
		PreparedStatement ps = null;
		
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.text as text, ");
			sql.append("messages.subject as subject, ");
			sql.append("messages.category as category, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("where messages.created_date >= ? and messages.created_date <= ? ");
			if (StringUtils.isNotEmpty(search_category)){
				sql.append("and category like ? ");
			}
			    ps = connection.prepareStatement(sql.toString());
			    ps.setString(1, firstDate + " 00:00:00");
			    ps.setString(2, secondDate + " 23:59:59");
			if (StringUtils.isNotEmpty(search_category)){
				ps.setString(3, String.format("%%%s%%", search_category));
			}
			ResultSet rs = ps.executeQuery();
			return toUserMessageList(rs);

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs) throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserMessage message = new UserMessage();
				message.setId(id);
				message.setLoginId(loginId);
				message.setName(name);
				message.setUserId(userId);
				message.setSubject(subject);
				message.setText(text);
				message.setCategory(category);
				message.setCreatedDate(createdDate);

				ret.add(message);

			}
			return ret;
		} finally {
			close(rs);
		}
	}
	
	public void deleteUserMessages(Connection connection, String messageDelete) {

		PreparedStatement ps = null;
		try {
			String sql = "DELETE  FROM  messages WHERE id=?";
			ps = connection.prepareStatement(sql);

			ps.setString(1, messageDelete);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}