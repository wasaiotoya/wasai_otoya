package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_code");
			sql.append(", department_code");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // login_id
			sql.append(", ?"); // password
			sql.append(", ?"); // name
			sql.append(", ?"); // branch_code
			sql.append(", ?"); // department_code
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchCode());
			ps.setInt(5, user.getDepartmentCode());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User editUser) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  login_id = ?");
			sql.append(", name = ?");
			if(StringUtils.isNotEmpty(editUser.getPassword())){
				sql.append(", password = ?");
			}
			sql.append(", branch_code = ?");
			sql.append(", department_code = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			int i=0;
			ps.setString(++i, editUser.getLoginId());
			ps.setString(++i, editUser.getName());
			if(StringUtils.isNotEmpty(editUser.getPassword())){
				ps.setString(++i, editUser.getPassword());
			}
			ps.setInt(++i, editUser.getBranchCode());
			ps.setInt(++i, editUser.getDepartmentCode());
			ps.setInt(++i, editUser.getId());
			System.out.println(editUser.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void pauseUser(Connection connection, String pauseUser) {

		PreparedStatement ps = null;
		try {
			String sql = ("UPDATE users SET  pause_user = 1 WHERE  id = ?");

			ps = connection.prepareStatement(sql);

			ps.setString(1, pauseUser);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
	
	public void reviveUser(Connection connection, String reviveUser) {

		PreparedStatement ps = null;
		try {

			String sql = "UPDATE users SET  pause_user = 0 WHERE  id = ?";
			ps = connection.prepareStatement(sql);

			ps.setString(1, reviveUser);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public User getLoginUser(Connection connection, String loginId, String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ?  AND password = ? AND pause_user=0" ;

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUsersList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	public User getEditUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUsersList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<User> getUsers(Connection connection) {
		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM users  ";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUsersList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
	private List<User> toUsersList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchCode = rs.getInt("branch_code");
				int departmentCode = rs.getInt("department_code");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");
				String pauseUser =  rs.getString("pause_user");

				User users = new User();
				users.setId(id);
				users.setLoginId(loginId);
				users.setPassword(password);
				users.setName(name);
				users.setBranchCode(branchCode);
				users.setDepartmentCode(departmentCode);
				users.setCreatedDate(createdDate);
				users.setUpdatedDate(updatedDate);
				users.setPauseUser(pauseUser);

				ret.add(users);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}