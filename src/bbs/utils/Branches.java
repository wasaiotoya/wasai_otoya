package bbs.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.common.collect.ImmutableMap;

public final class Branches {
	
	private static final Map<Integer, String> map = ImmutableMap.of(
			1, "本店",
			2, "支店"
	);
	
	public static Set<Integer> keySet() {
		return map.keySet();
	}
	
	public static Set<Entry<Integer, String>> entrySet() {
		return map.entrySet();
	}
	
	public static Collection<String> values() {
		return map.values();
	}
	
	public static Map<Integer, String> getInstance() {
		return new HashMap<>(map);
	}

}
