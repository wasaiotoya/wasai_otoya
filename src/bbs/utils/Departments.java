package bbs.utils;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

public final class Departments {
	private static final Map<Integer, String> map = ImmutableMap.of(
			1, "総務",
			2, "店長",
			3, "社員",
			4, "情報"
	);
	
	public static Map<Integer, String> getInstance() {
		return map;
	}
	
}
